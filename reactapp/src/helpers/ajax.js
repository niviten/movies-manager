const URL_PREFIX = 'http://localhost:3333'
const URL_PREFIX_API_JSON = 'http://localhost:3333/api/json'

export default function ajax(url, requestData, defaultPrefix) {
    if (!requestData) {
        requestData = {}
    }
    const urlPrefix = defaultPrefix ? URL_PREFIX: URL_PREFIX_API_JSON
    url = urlPrefix + url
    return new Promise((resolve, reject) => {
        fetch(url, {
            method: 'POST',
            body: JSON.stringify(requestData),
            headers: {'Content-Type': 'application/json'},
            credentials: 'include'
        }).then((response) => {
            return response.json()
        }).then((responseDataAsJSON) => {
            resolve(responseDataAsJSON)
        }).catch((err) => {
            reject(err)
        })
    })
}
