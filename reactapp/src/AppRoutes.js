import {
    HashRouter as Router,
    Routes,
    Route
} from 'react-router-dom';
import App from './App';
import Browse from './pages/browse/Browse';
import Home from './pages/home/Home';

export default function AppRoutes() {
    return <>
        <Router>
            <Routes>
                <Route path='/' element={<App />} >
                    <Route path='/' element={<Home />} />
                    <Route path='/browse' element={<Browse />} />
                </Route>
            </Routes>
        </Router>
    </>
}
