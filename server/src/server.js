const express = require('express')
const cors = require('cors')
const port = 3333
const app = express()
app.use(express.json())
app.use(cors({
    origin: 'http://localhost:3000',
    methods: ['POST', 'GET'],
    credentials: true
}))

app.all('/test', (req, res) => {
    const { name, age } = req.body
    res.send(JSON.stringify({
        message: `hi ${name}, your age is ${age}`
    }))
})

const moviesAPI = require('./api/movies_api')
app.all('/api/json/movies/get', moviesAPI.getMovies)

app.listen(port, () => {
    console.log(`Server started @ ${port}`)
})
