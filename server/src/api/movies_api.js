const { executeQuery } = require('../helpers/database')

function getMovies(req, res) {
    const query = 'SELECT m.NAME AS movieName, md.NAME AS directorName, ml.NAME AS movieLanguage, m.YEAR_RELEASED AS yearReleased, m.ADDITIONAL_INFO AS additionalInfo FROM Movie m INNER JOIN MovieDirector md ON m.DIRECTOR_ID = md.ID INNER JOIN MovieLanguage ml ON m.LANGUAGE_ID = ml.ID'
    executeQuery(query).then(rows => {
        res.send(rows)
    })
}

module.exports = {
    getMovies
}
