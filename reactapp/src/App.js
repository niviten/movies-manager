import { useEffect, useState } from 'react';
import {
    Outlet,
    Link,
    useLocation
} from 'react-router-dom';

const NAV_ITEMS = [
    { name: 'Home', path: '/' },
    { name: 'Browse', path: '/browse' }
]

export default function App() {
    const location = useLocation()
    const [ activeNavItemIndex, setActiveNavItemIndex ] = useState(() => {
        return NAV_ITEMS.findIndex(navItem => navItem.path === location.pathname)
    })
    useEffect(() => {
        setActiveNavItemIndex(NAV_ITEMS.findIndex(navItem => navItem.path === location.pathname))
    }, [location])

    return <>
        <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
            <Link to='/' className='navbar-brand'>Movie Manager</Link>
            <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
                <span className='navbar-toggler-icon'></span>
            </button>

            <div className='collapse navbar-collapse' id='navbarSupportedContent'>
                <ul className='navbar-nav mr-auto'>
                    {NAV_ITEMS.map((navItem, index) => {
                        return <li className='nav-item' onClick={() => setActiveNavItemIndex(index)}>
                            <Link to={navItem.path} className={(index === activeNavItemIndex) ? 'nav-link active': 'nav-link'}>{ navItem.name }</Link>
                        </li>
                    })}
                </ul>
            </div>
        </nav>
        <Outlet />
    </>
}
